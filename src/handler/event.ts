import mindsService from '../service/minds';

const eventHandler = async (ws: WebSocket, event: Object) => {
  // TODO: any errors that need to be thrown?
  await mindsService.putEvent(ws, event);
};

export default eventHandler;
