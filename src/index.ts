import { WebSocketServer } from 'ws';

import SOCKET from './enum/socket';
import CLOSE_CODES from './enum/closeCodes';
import handler from './handler/index';
import { formatNotice } from './helper/format-event';

const wss = new WebSocketServer({ port: parseInt(process.env.SERVER_PORT) });

console.log(`PID: ${process.pid}`);
console.log(
  `Listening on port: ${process.env.SERVER_PORT} and path: ${process.env.SERVER_PATH}`
);

// Open socket and pass event to the event handler
wss.on(SOCKET.CONNECTION, (ws, req) => {
  // Active subscriptions
  const subs = new Map();

  // On Error
  ws.on(SOCKET.ERROR, (error) => {
    console.error(`Oops! Recieved this error: ${error}`);
  });

  // On Message
  ws.on(SOCKET.MESSAGE, (data) => {
    try {
      const buf = Buffer.from(data);
      const message = buf.toString();

      console.log(`[RECV]: ${message}`);
      handler(ws, buf.toString(), subs);
    } catch (err) {
      console.error(err);
      ws.send(formatNotice('[ERROR]: Failed to parse message as a string!'));
    }
  });

  // While connection is open, periodically poll subscriptions
  const interval = setInterval(() => {
    subs.forEach(({ query }, key) => {
      console.log(
        `[POLL]: remote ${req.socket.remoteAddress}, subscription ${key}, query ${query}`
      );
      handler(ws, query, subs);
    });
  }, parseInt(process.env.LIVE_POLLING_INTERVAL));

  // On Close
  ws.on('close', () => {
    clearInterval(interval);
  });
});

// On TERM signal, close open connections with a Service Restart code
process.on('SIGTERM', (signal) => {
  console.log('Got signal ' + signal);
  wss.clients.forEach((ws) => {
    ws.close(CLOSE_CODES.SERVICE_RESTART, 'Service Restart');
  });
  process.exit();
});
