import axios from 'axios';
import { formatNotice } from '../helper/format-event';

const baseUri = process.env.MINDS_BASE_URI;

const getReq = async (ws: WebSocket, filters: string[]) => {
  try {
    // Get the events from the engine endpoint
    const response = await axios.get(`${baseUri}/api/v3/nostr/req`, {
      params: { filters }
    });
    return response.data;
  } catch (err) {
    console.error(
      `[ERROR]: Error when retrieving events from Minds backend! Status: ${err.response?.status}, Message: ${err.response?.data?.message}`
    );
    ws.send(
      formatNotice(
        '[ERROR]: Failed to retrieve events for the provided filters!'
      )
    );
  }
};

const putEvent = async (ws: WebSocket, event: Object) => {
  try {
    // Get the event from the engine endpoint
    const response = await axios.put(`${baseUri}/api/v3/nostr/event`, event);
    return response.data;
  } catch (err) {
    console.error(
      `[ERROR]: Error when persisting event to Minds backend! Status: ${err.response?.status}, Message: ${err.response?.data?.message}`
    );

    if (err.response) {
      ws.send(formatNotice(`[ERROR]: ${err.response?.data?.message}`));
    } else {
      ws.send(
        formatNotice('[ERROR]: Failed to persist event to Minds backend!')
      );
    }
  }
};

export default {
  getReq,
  putEvent
};
